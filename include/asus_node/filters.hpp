#ifndef FILTERS_HPP_
#define FILTERS_HPP_

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

namespace filters {
	/** 
	 * Do a Gaussian Blur of the image.
	 * 
	 * @param src The source image.
	 * @param gaussian_blur The blurred image. 
	 */
	void gaussianBlur(const cv::Mat& src, cv::Mat& gaussian_blur);

    /** 
	 * Do a Median Blur of the image. 
	 * 
	 * @param src The source image.
	 * @param median_blur The blurred image.
	 */
	void medianBlur(const cv::Mat& src, cv::Mat& median_blur);

	/** 
	 * Do a Bilateral Blur of the image.
	 * 
	 * @param src The source image.
	 * @param bilateral_blur The blurred image.
	 */
	void bilateralBlur(const cv::Mat& src, cv::Mat& bilateral_blur);

	/** 
	 * Compute the median image of a vector of images.
	 * 
	 * @param srcs The source images.
	 * @param median_image The median of all source images.
	 */
	void medianImage(const std::vector<cv::Mat>& srcs, cv::Mat& median_image);

	/** 
	 * Compute the average (mean) image of a vector of images.

	 * 
	 * @param srcs The source images.
	 * @param average_image The mean of all source images.
	 */
	void averageImage(const std::vector<cv::Mat> srcs, cv::Mat& average_image);

	/** 
	 * Unused for now. From opencv image smoothing tutorial.
	 * 
	 * @param caption Caption to display.
	 * @param src Source image.
	 * @param dst Destination image.
	 * @param window_name The name of the window.
	 * 
	 * @return 0 for success.
	 */
	int display_caption( char* caption, const cv::Mat& src, cv::Mat& dst, const std::string& window_name );

	/** 
	 * From opencv image smoothin tutorial. Not used for now.
	 * 
	 * @param delay The delay time for the opencv imshow.
	 * @param src The source image.
	 * @param dst The destination image.
	 * @param window_name The name of the window on which to display this.
	 * 
	 * @return 0 for success.

	 */
	int display_dst( int delay, const cv::Mat& src, cv::Mat& dst, const std::string& window_name );

	/** 
	 * Calculates the median of a vector of doubles.
	 * 
	 * @param values The double values for which we want to compute the median.
	 * 
	 * @return The median of the values.
	 */
	double CalcMedian(std::vector<double> values);
}

#endif
