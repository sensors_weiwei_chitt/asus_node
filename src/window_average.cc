#include <numeric>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/Float32.h>
#include <sensor_msgs/Image.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <asus_node/filters.hpp>

class WindowProcess {
	/**
	 * A NodeHandle to control this node.
	 * 
	 */
	ros::NodeHandle nh_;

	/**
	 * Ros publisher for mean
	 * 
	 */
	ros::Publisher mean_pub_;

	/**
	 * ROS publisher for variance.
	 */
	ros::Publisher var_pub_;

	/**
	 * ROS Subscriber to window.
	 */
	ros::Subscriber window_sub_;

	/**
	 * A vector to hold the variance range. 
	 */
	std::vector <double> ranges_;

	/**
	 * What filter to apply before calculating depth.
	 */
	std::string filter_type;

public:
	bool reached_290;
	WindowProcess(const std::string& filter = ""):
		reached_290 (false),
		filter_type (filter) {
		// The Window topic gives us sensor_msgs/Image data; So
		// callback should have this as argument.
		window_sub_ = nh_.subscribe("window", 1, &WindowProcess::windowCallback, this);
		mean_pub_ = nh_.advertise<std_msgs::Float32> ("depth/mean", 1);
		var_pub_ = nh_.advertise <std_msgs::Float32> ("depth/std", 1);
		
	}

private:
	void windowCallback(const sensor_msgs::Image::ConstPtr& image_ptr) {
		cv_bridge::CvImagePtr image_as_mat = cv_bridge::toCvCopy(image_ptr);
		cv::Mat src = image_as_mat->image.clone();

		// We shouldn't need this. We have made sure that the distances aren't nan-s.
		/*
		for(int i = 0; i < src.rows; i++) {
			for(int j = 0; j < src.cols; j++) {
				if(isnan(src.at<float>(i,j)) || isinf(src.at<float>(i,j))) {
					src.at<float>(i,j) = 0.0;
				}
			}
		}
		*/
		
		cv::Mat resultImage = src.clone();
		double mean = 0.0;
		double total = 0.0;

		// Added for last part. We need to apply filters on the data.
		if(filter_type.compare("bilateral") == 0) {
			filters::bilateralBlur(image_as_mat->image, resultImage);
		}
		else if(filter_type.compare("median") == 0) {
			filters::medianBlur(image_as_mat->image, resultImage);
		}
		else if(filter_type.compare("gaussian") == 0) {
			filters::gaussianBlur(image_as_mat->image, resultImage);
		}

		ROS_INFO("Filtering done...");

		// Copied from Weiwei's code.
		for (int i=0; i < resultImage.rows; i++) {
         	for (int j=0; j < resultImage.cols; j++) {
			  	float value = resultImage.at<float>(i,j);
				if(!isnan(value) && !isinf(value)) {
				    mean += value;
					total = total + 1.0;
				}
        	}
		}

		mean = mean/(total);

		ROS_INFO("Mean: %lf", mean);
		if(ranges_.size() < 290) {
			ranges_.push_back(mean);
		}
		else
			reached_290 = true;

		std_msgs::Float32 mean_msg, var_msg;
		mean_msg.data = static_cast<float> (mean);
		this->mean_pub_.publish(mean_msg);

		double var = 0.0;
		total = 0.0;
		// Variance
		for (int i=0; i < resultImage.rows; i++) {
         	for (int j=0; j < resultImage.cols; j++) {
				float value = resultImage.at<float>(i,j);
				if(!isnan(value) && !isinf(value)) {
					var = var + (pow(mean - value, 2));
					total = total + 1.0;
				}
        	}
		}
		var=var/(total);
		var_msg.data = static_cast<float> (sqrt(var));
		this->var_pub_.publish(var_msg);

		ROS_INFO("STD: %lf", sqrt(var));
		ROS_INFO("%lu Ranges acquired", ranges_.size());
	}

public:

	double getRangeMean() {
		return std::accumulate(ranges_.begin(), ranges_.end(), 0.0)/ranges_.size();		
	}

	double sqOfDiff(const double& a, const double& b) {
		return pow(a-b, 2);
	}

	double getRangeVariance() {
		double mean = getRangeMean();
		std::vector <double> var(ranges_.size());
		std::transform(ranges_.begin(), ranges_.end(), var.begin(), boost::bind(&WindowProcess::sqOfDiff, this, _1, mean));
		return std::accumulate(var.begin(), var.end(), 0.0)/var.size();
	}
};

int main(int argn, char* args[]) {

	ros::init(argn, args, "window_process");

	WindowProcess wp(args[1]);
	while(ros::ok()) {
		ros::spinOnce();
		if(wp.reached_290) {
			break;
		}
	}

	ROS_INFO("Final Range Mean for 300 range readings: %lf", wp.getRangeMean());
	ROS_INFO("Final Range Var for 300 range readings: %.10lf", wp.getRangeVariance());
	ROS_INFO("Final Range Standard Deviation for 300 range readings: %.10lf", sqrt(wp.getRangeVariance()));
	ROS_INFO("%s", args[1]);

	return 0;
}
