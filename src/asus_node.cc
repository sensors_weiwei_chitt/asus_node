/*
 * This source file is part of the Sensors and Sensing course at AASS.
 * If you use this material in your courses or research, please include 
 * a reference.
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, AASS Research Center, Orebro University.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of AASS Research Center nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */

//include headers from ROS
#include <ros/ros.h>
#include <pcl_conversions/pcl_conversions.h>
#include "sensor_msgs/PointCloud2.h"
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
//PCL
#include "pcl/point_cloud.h"
#include "pcl/io/pcd_io.h"
//EIGEN
#include <Eigen/Eigen>
//OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <asus_node/filters.hpp>
//OPENCV Window names
#define RGB_WINDOW "RGB Image"
#define DEPTH_WINDOW "Depth Image"

#include <std_msgs/Int16.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>

//Your Node Class
class AsusNode {
protected:
	sensor_msgs::PointCloud2::ConstPtr points_;
private:
	// window size for cutting image.
	int a;
	// Our NodeHandle, points to home
	ros::NodeHandle nh_;
	//global node handle
	ros::NodeHandle n_;

	//Subscribers for topics
	ros::Subscriber points_sub_;
	ros::Subscriber depth_sub_;
	ros::Subscriber rgb_sub_;

	//topics to subscribe to
	std::string subscribe_topic_point;
	std::string subscribe_topic_depth;
	std::string subscribe_topic_color;

	ros::Subscriber window_size_sub_;

	ros::Publisher window_pub_;
	ros::Publisher bilateral_blur_pub_;
	ros::Publisher gaussian_blur_pub_;
	ros::Publisher median_blur_pub_;

	std::string filter_type;

	std::vector <cv::Mat> src_images_;

	void windowSizeCallback(const std_msgs::Int16::ConstPtr& msg) {
		a = msg->data;
		ROS_INFO("Window size has been changed to %d", a);
	}
	
public:
	AsusNode(const std::string& filter):
		a(20),
		filter_type(filter) {

		nh_ = ros::NodeHandle("~");
		n_ = ros::NodeHandle();

		window_size_sub_ = n_.subscribe("window_size", 1, &AsusNode::windowSizeCallback, this);
		window_pub_ = n_.advertise<sensor_msgs::Image>("window", 1);

		// Advertise three different images on three topics for the blur.
		bilateral_blur_pub_ = n_.advertise<sensor_msgs::Image>("bilateral_blur", 1);
		gaussian_blur_pub_ = n_.advertise<sensor_msgs::Image>("gaussian_blur", 1);
		median_blur_pub_ = n_.advertise<sensor_msgs::Image>("median_blur", 1);

		//read in topic names from the parameter server
		//nh_.param<std::string>("points_topic",subscribe_topic_point,"/camera/depth_registered/points");
		nh_.param<std::string>("depth_topic",subscribe_topic_depth,"/camera/depth_registered/image_raw");
		//nh_.param<std::string>("rgb_topic",subscribe_topic_color,"/camera/rgb/image_raw");

		//subscribe to topics
		//points_sub_ = n_.subscribe(subscribe_topic_point, 1, &AsusNode::points2Callback, this);
		depth_sub_ = n_.subscribe(subscribe_topic_depth, 1, &AsusNode::depthCallback, this);
		//rgb_sub_ = n_.subscribe(subscribe_topic_color, 1, &AsusNode::rgbCallback, this);

	}

	// Callback for pointclouds
	void points2Callback(const sensor_msgs::PointCloud2::ConstPtr& msg_in)
	{
		points_ = msg_in;
	}

	//callback for rgb images
	void rgbCallback(const sensor_msgs::Image::ConstPtr& msg)
	{
		cv_bridge::CvImageConstPtr bridge;
		try
		{
			bridge = cv_bridge::toCvCopy(msg, "bgr8");
		}
		catch (cv_bridge::Exception& e)
		{
			ROS_ERROR("Failed to transform rgb image.");
			return;
		}
		/* do something colorful"*/
//		cv::imshow(RGB_WINDOW, bridge->image);
		//cv::imwrite("rgb_image.png", bridge->image);
//		cv::waitKey(1);
	}

	//callback for depth images
	void depthCallback(const sensor_msgs::Image::ConstPtr& msg)
	{
		cv_bridge::CvImageConstPtr bridge;
		try
		{
			bridge = cv_bridge::toCvCopy(msg, "32FC1");
		}
		catch (cv_bridge::Exception& e)
		{
			ROS_ERROR("Failed to transform depth image.");
			return;
		}

		// Extract a square window of size 2a by 2a.
		// cv::Mat window = bridge->image( cv::Range(239-a, 240+a), cv::Range(319-a,320+a) );

		cv_bridge::CvImagePtr window_image(new cv_bridge::CvImage(bridge->header,
					bridge->encoding,
					bridge->image( cv::Range(239-a, 240+a), cv::Range(319-a,320+a))));;

		// Publish a boost shared pointer. That way, we have no copying cost.
		window_pub_.publish(window_image->toImageMsg());


		cv_bridge::CvImagePtr resultImage(new cv_bridge::CvImage(bridge->header,
																		bridge->encoding,
																		bridge->image));

		
		cv::Mat src = bridge->image.clone();
		filterImage(src);
		
		if(filter_type.compare("gaussian") == 0) {
			ROS_INFO("Now applying GAUSSIAN BLUR filter.");
			saveImage(src, "depth_image.jpg");
			filters::gaussianBlur(src, resultImage->image);
			saveImage(resultImage->image, "gaussian_blur.jpg");
			exit(0);
		}
		
		else if(filter_type.compare("median_blur") == 0) {
			ROS_INFO("Now applying MEDIAN BLUR filter.");
			saveImage(src, "depth_image.jpg");
			filters::medianBlur(src, resultImage->image);
			saveImage(resultImage->image, "median_blur.jpg");
			exit(0);
		}
		
		else if(filter_type.compare("bilateral") == 0) {
			ROS_INFO("Now applying BILATERAL BLUR filter.");
			saveImage(src, "depth_image.jpg");
			filters::bilateralBlur(src, resultImage->image);
			saveImage(resultImage->image, "bilateral_blur.jpg");
			exit(0);
		}

		if(src_images_.size() < 10) {
			ROS_INFO("Aggregating %lu images.", src_images_.size());
			src_images_.push_back(src);
		}
		else {
			ROS_INFO("Got 10 images.");
			if(filter_type.compare("average") == 0) {
				ROS_INFO("Now applying AVERAGE filter over 10 images.");
				filters::averageImage(src_images_, resultImage->image);
				saveImage(resultImage->image, "average_image.jpg");
				exit(0);
			}
			else {
				ROS_INFO("Now applying MEDIAN filter over 10 images.");
				filters::medianImage(src_images_, resultImage->image);
				saveImage(resultImage->image, "median_image.jpg");
				exit(0);
			}
		}
	}
	
	void saveImage(const cv::Mat& image, const std::string& file_name){
		cv::Mat color, color_scaled;
		// Scaling the depth. 8m is the max value of depth visible in this image.
		cv::Mat(image-0.0).convertTo(color_scaled, CV_8UC1, 255/8);
		cv::cvtColor(color_scaled, color, CV_GRAY2RGB);
		cv::imwrite(file_name.c_str(), color);
	}

	void filterImage(cv::Mat& input) {
		for(int i = 0; i < input.rows; i++) {
			for(int j = 0; j < input.cols; j++) {
				if(isnan(input.at<float>(i,j)) || isinf(input.at<float>(i,j))) {
					input.at<float>(i,j) = 0.0;
				}
			}
		}
	}

	void processPointCloud() {
		if(!points_) {
			ROS_INFO("No point cloud available.");
			return;
		}

		pcl::PointCloud<pcl::PointXYZ> cloud;
		pcl::fromROSMsg (*points_, cloud);

//		savePCD("sample1.pcd", cloud);

		/* do something pointy"*/
		ROS_INFO_STREAM("Got cloud with "<<cloud.size()<<" points");

		points_.reset();

		//exit(0);
	}

	void savePCD(std::string file_name, pcl::PointCloud<pcl::PointXYZ>& cloud) {
		pcl::io::savePCDFileASCII ("sample1.pcd", cloud);
		ROS_INFO("Saved %lu data points to sample1.pcd.", cloud.points.size ());
	}

};

//main function
int main(int argc, char **argv) {
	ros::init(argc, argv, "asus_node");

	std::cerr<<"creating node\n";
	AsusNode nd(argv[1]);
	std::cerr<<"node done\n";

	while(ros::ok()) {
		ros::spinOnce();
		//nd.processPointCloud();
	}

	return 0;
}

