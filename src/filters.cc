#include <numeric>
#include <asus_node/filters.hpp>

int DELAY_CAPTION = 1500;
int DELAY_BLUR = 100;
int MAX_KERNEL_LENGTH = 31;

namespace filters {

	void gaussianBlur(const cv::Mat& src, cv::Mat& gaussian_blur){   
			cv::GaussianBlur( src, gaussian_blur, cv::Size( 31, 31 ), 0, 0 );
	}

	void medianBlur(const cv::Mat& src, cv::Mat& median_blur){
  
		 medianBlur ( src, median_blur, 5 ); 
	}


	void bilateralBlur(const cv::Mat& src, cv::Mat& bilateral_blur){
		bilateralFilter ( src, bilateral_blur, 3, 6, 3/2 );
	}

	void medianImage(const std::vector<cv::Mat>& srcs, cv::Mat& median_image){

		std::vector<double> temp;
        
		for (int i=0; i<480; i++){
			for (int j=0; j<640; j++){
				temp.clear();
				for (int z=0; z<10; z++){
					temp.push_back(srcs[z].at<float>(i,j));
				}
				median_image.at<float>(i,j)=CalcMedian(temp);
			}
		}
	}


	void averageImage(const std::vector<cv::Mat> srcs, cv::Mat& average_image){

		std::vector<double> temp;
        
		for (int i=0; i<480; i++){
			for (int j=0; j<640; j++){
				temp.clear();
				for (int z=0; z<10; z++){
					temp.push_back(srcs[z].at<float>(i,j));
				}
				average_image.at<float>(i,j)= std::accumulate(temp.begin(), temp.end(), 0.0)/temp.size();
			}
		}
	}

	int display_caption( char* caption, const cv::Mat& src, cv::Mat& dst, const std::string& window_name ){
		dst = cv::Mat::zeros( src.size(), src.type() );
		putText( dst, caption,
				 cv::Point( src.cols/4, src.rows/2),
				 CV_FONT_HERSHEY_COMPLEX, 1, cv::Scalar(255, 255, 255) );

		imshow( window_name, dst );
		int c = cv::waitKey( DELAY_CAPTION );
		if( c >= 0 ) { return -1; }
		return 0;
	}

	int display_dst( int delay, const cv::Mat& src, cv::Mat& dst, const std::string& window_name ){
		imshow( window_name, dst );
		int c = cv::waitKey ( delay );
		if( c >= 0 ) { return -1; }
		return 0;
	}
  
	double CalcMedian(std::vector<double> values){

		double median;
		size_t size = values.size();

		std::sort(values.begin(), values.end());

		if (size  % 2 == 0){
			median = (values[size / 2 - 1] + values[size / 2]) / 2;
		}
		else {
			median = values[size / 2];
		}
		return median;
	}

}
