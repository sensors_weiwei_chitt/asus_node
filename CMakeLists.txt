cmake_minimum_required(VERSION 2.8.3)
project(asus_node)

find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  sensor_msgs
  roscpp
  pcl_ros 
  )

find_package(OpenCV REQUIRED)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES filters
  CATKIN_DEPENDS sensor_msgs roscpp pcl_ros cv_bridge
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

add_library(filters src/filters.cc)
target_link_libraries(filters
  ${catkin_LIBRARIES}
  ${OpenCV_LIBRARIES}
  )

add_executable(asus_node src/asus_node.cc)
add_executable(window_process src/window_average.cc)

target_link_libraries(asus_node
  ${catkin_LIBRARIES}
  ${OpenCV_LIBRARIES}
  filters
)

target_link_libraries(window_process
  ${catkin_LIBRARIES}
  ${OpenCV_LIBRARIES}
  filters
)
